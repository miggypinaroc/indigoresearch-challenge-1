const randomizer = () => {
  let min = parseFloat(document.getElementById("min").value, 10);
  let max = parseFloat(document.getElementById("max").value, 10);

  document.getElementById("randnum").innerHTML = Math.floor(
    Math.random() * (max - min + 1) + min
  );
};
//src https://stackoverflow.com/questions/38126901/random-number-js-min-max
