// src
// https://www.sitepoint.com/build-a-simple-web-server-with-node-js/
// https://developer.mozilla.org/en-US/docs/Learn/Server-side/Node_server_without_framework
const http = require("http");
const path = require("path");
const fs = require("fs");
const hostname = "localhost";
const port = 5000;

const server = http.createServer((req, res) => {
  console.log("request ", req.url);
  let filePath = "." + req.url;
  if (filePath == "./") {
    filePath = "index.html";
  }

  fs.exists(filePath, exists => {
    if (!exists) {
      res.statusCode = 404;
      res.end();
      return;
    }

    fs.readFile(filePath, (err, data) => {
      if (err) throw err;
      res.end(data, "utf-8");
    });
  });
});

server.listen(port, hostname, () => {
  console.log(`Server running at http://${hostname}:${port}/`);
});

const randomizer = () => {
  let min = parseFloat(document.getElementById("min").value, 10);
  let max = parseFloat(document.getElementById("max").value, 10);

  document.getElementById("randnum").innerHTML = Math.floor(
    Math.random() * (max - min + 1) + min
  );
};
