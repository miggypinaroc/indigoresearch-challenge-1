# Comments

## File: server.js
Good job. Nice touch adding the sources at the top. 

```js
const port = 5000;
```

**Follow instructions.** Use the specified port `3000`.

```js
fs.readFile(filePath, (err, data) => {
    if (err) throw err;
    res.end(data, "utf-8");
});
```

Error handling is an important thing to learn in programming. If you get a file read error, your server locks up and it doesnt execute the `res.end(data, "utf-8");`. Your client will be left waiting for a response. You should implement a 503 response instead of just running `throw err`. 

```js
const randomizer = () => {
    let min = parseFloat(document.getElementById("min").value, 10);
    let max = parseFloat(document.getElementById("max").value, 10);

    document.getElementById("randnum").innerHTML = Math.floor(
    Math.random() * (max - min + 1) + min
    );
};
```

You should remove this randomizer from your server. 

## File: .gitkeep

```bash
rm .gitkeep
git rm .gitkeep
```

Remove `.gitkeep` files if the folder isn't meant to be empty.

## File: index.html

```html
<script src="./assets/main.js"></script>
<script src="server.js"></script>
```

Do not import your `server.js` file. Learn to follow a development paradigm like MVC. This is a big security risk.

```html
<button onclick="randomizer()" method="GET">GO!</button>
```

While it is okay to use the `onclick` attribute. Be mindful when you import your `main.js`. In your HTML, this `onclick` was defined before `main.js` was imported. In most cases, this wont pose as a problem, but just keep this in mind in case you get `undefined` errors. 

## File: main.js

While it's usually okay to copy code off of the internet, I hope you know what `parseFloat` means. Aside from that, it works as it should.

## Design

Font-sizes, paddings, margins, and some content were not followed. Being a good front-end developer also means having a keen eye for design. 

## End notes

Follow instructions and clarify things when needed. Read more about the basics of Javascript and web development. 
